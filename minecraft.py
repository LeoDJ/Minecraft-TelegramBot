﻿# -*- coding: utf-8 -*-
import telebot
from telebot import types
from telebot import *
import time

import settings
from settings import *
settings.init()

from mDB import *
db = connect(dbPath)

from emojis import *
emoji = emoji()


from serverPing import *
server = server(db)



	
hideBoard = types.ReplyKeyboardHide(selective=True)
forceReply = types.ForceReply(selective=True)

tmp = {}
lastMsg = {}
lastIpSelect = {}


	#When new messages arrive TeleBot will call this function.		
def listener(messages):
	for m in messages:
		uid = m.chat.id
		if m.content_type == 'text':
			#print the sent message to the console
			print str(m.from_user.username) + " [" + str(m.chat.id) + "]: " + m.text.encode('utf-8')
			lastMsg[uid] = m


bot = telebot.TeleBot(TOKEN)
bot.set_update_listener(listener) #register listener
bot.polling(none_stop = True)

import serverPoll
poller = serverPoll.poller(db, server, bot)
poller.start()

def invalidInput(uid):
	bot.send_message(uid, "Invalid input, please try again!")
	
def getIpSel(uid, withManual):
	ipSel = types.ReplyKeyboardMarkup(one_time_keyboard=True, selective=True)
	if withManual:
		ipSel.row("[0] Enter IP manually")
	favs = db.fav.get(uid)
	first = True
	firstIp = ""
	for f in favs:
		ipString = "[" + f[0] + "] " + f[1]
		if first:
			first = False
			firstIp = ipString
		else:
			first = True
			ipSel.row(firstIp, ipString)
	if not first:
		ipSel.add(firstIp)
	return ipSel

def processIpSel(uid, sel):
	if sel.isnumeric():
		return db.fav.getIP(uid, sel)
	else:
		try:
			return sel.split('] ')[1]
		except:
			invalidInput(uid)
			return -1


#= = = = = = = = = = = = = = = = = = = = =     Start     = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
@bot.message_handler(commands=['start'])
def comStart(m):
	uid = m.chat.id
	user = m.from_user
	
	if(db.user.isRegistered(uid)):
		print "User",m.from_user.username,"already registered - resetting entry..."
	db.user.register(m.chat)
	bot.send_message(uid, "Hello "+user.username+", I'm the almighty Minecraft Bot, let's get started, shall we?")
	comHelp(m)
	
@bot.message_handler(commands=['stop'])
def comStop(m):
	print "Closing database"
	db.close()
	
@bot.message_handler(commands=['state'])
def comState(m):
	uid = m.chat.id
	state = db.user.getState(uid)
	if state != "":
		bot.send_message(uid, state)
	else:
		bot.send_message(uid, "\"\"")

#= = = = = = = = = = = = = = = = = = = = =     Help     = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
@bot.message_handler(commands=['help'])
def comHelp(m):
	helpText = helpTextPre
	for c in commands:
		helpText = helpText + "\n/" + c[0] + " - " + c[1]
	helpText += helpTextAfter
	bot.send_message(m.chat.id, helpText)
	
#= = = = = = = = = = = = = = = = = = = = =     About     = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
@bot.message_handler(commands=['about'])
def comAbout(m):
	bot.send_message(m.chat.id, aboutText)


#= = = = = = = = = = = = = = = = = = = = =     Feedback     = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
def receiveFeedback(m):
	fbText = m.from_user.username
	fbText += " ["+str(m.chat.id) + "]: "
	fbText += m.text.encode('utf-8')
	fbText += "\n"
	open(feedbackFile, "a").write(fbText)
	bot.send_message(m.chat.id, "Thanks for your feedback")

@bot.message_handler(commands=['feedback'])
def comFeedback(m):
	uid = m.chat.id
	msg = m.text
	
	if len(msg) > len("/feedback"):
		m.text = m.text[10:]
		receiveFeedback(m)
	else:
		db.user.setState(uid, "feedback1")
		bot.send_message(uid, "Please write your feedback now:")

@bot.message_handler(func=lambda m: db.user.getState(m.chat.id) == "feedback1")
def comFeedback1(m):
	uid = m.chat.id
	msg = m.text
	
	receiveFeedback(m)
	
	db.user.setState(uid, "")
	
		

	
#= = = = = = = = = = = = = = = = = = = = =     Ping     = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
def comPingServer0(ip, uid):
	if len(ip) <= 3 and ip.isdigit():
		ip = db.fav.getIP(uid, ip)
		if ip == None:
			bot.send_message(uid, "Unknown favID")
	#bot.send_message(uid, "Contacting server...")
	bot.send_chat_action(uid, 'typing')
	if (server.ping(ip) != 0):
		bot.send_message(uid, "Error occured during pinging, maybe check the server address")
	else:
		bot.send_message(uid, server.getData(ip))




@bot.message_handler(commands=['ping'])
def comPingServer1(m):		
	uid = m.chat.id
	msg = m.text
	
	if len(msg) > len("/ping"):
		params = msg.split(' ')
		if len(params) > 2:
			bot.send_message(uid, "Wrong Address format, please try again!")
		else:
			#tmp['ip'] = params[1]
			comPingServer0(params[1], uid)
				
	else:
		db.user.setState(uid, "ping1")
		bot.send_message(uid, "Please enter the IP adress of the server you wish to ping:")
	
@bot.message_handler(func=lambda m: db.user.getState(m.chat.id) == "ping1")
def comPingServer2(m):
	uid = m.chat.id
	if len(m.text.split(' ')) > 2:
		bot.send_message(uid, "Wrong Address format, please try again!")
	else:
		comPingServer0(m.text, uid)
		db.user.setState(uid, "")

		
	
	
#= = = = = = = = = = = = = = = = = = = = =     Favorites     = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
def testFav(uid):
	bot.reply_to(m, "Showing keyboard", reply_markup = getIpSel(uid, False))

def printFav(uid):
	msg = "Favorites: \n"
	favs = db.fav.get(uid)
	#for f in sql.select("SELECT pos, ip, mode FROM favs WHERE userID = ? ORDER BY pos", (uid,)):
	for f in favs:
		pos = str(f[0])
		ip = f[1].encode('utf-8')
		mode = f[2]
		
		icon = ""
		try:	
			if sData[ip]["online"]:
				icon += emoji.on
			else:
				icon += emoji.off
			if (settings.isAnySet(mode)):
				icon += emoji.bell
			else:
				if sData[ip]["queryable"]:
					icon += emoji.noBell
				else:
					icon += emoji.bad
			
		except KeyError as e:
			print "[printFav]",ip,"KeyError", e
			icon += emoji.hourglass
		
		msg += "\n[" + pos + "] " +icon + " " + ip
		
	if len(msg) < len("Favorites: \n   "):
		msg = "You don't have any favorites yet. Start adding some using /fav add"
	bot.send_message(uid, msg)


	

	
def editFav(uid):
	bot.send_message(uid, "Wörk in Progress")

	
	
favMenu = types.ReplyKeyboardMarkup(one_time_keyboard = True, selective=True)
favMenu.row("0. "+emoji.back+" Exit")
favMenu.row("1. "+emoji.plus+" Add Favorite", "2. "+emoji.minus+" Remove Favorite")
favMenu.row("3. "+emoji.bell+" Set Notification", "4. "+emoji.pen+" Edit Order")

def showFavMenu(m):
	uid = m.chat.id
	printFav(uid)
	db.user.setState(uid, "favMenu")
	bot.reply_to(m, "What would you like to do?", reply_markup = favMenu)

@bot.message_handler(commands=['fav'])
def comFav(m):
	uid = m.chat.id
	msg = m.text
	
	if msg != "/fav" and msg != "/fav@MinecraftToolBot":
		params = msg.split(' ')
		mode = params[1]
		
		if len(params) >= 3: #resolve ip from favID
			ip = params[2]
			if len(ip) <= 3 and ip.isdigit():
				ip = db.fav.getIP(uid, ip)
		
		if (mode == "show"):
			pass
		elif (mode == "add"):
			try:
				db.fav.add(uid, ip)
				printFav(uid)
			except UnicodeDecodeError:
				invalidInput(uid)
		elif (mode == "remove" or mode == "delete" or mode == "rm"):
			db.fav.remove(uid, ip)
			printFav(uid)
		elif (mode == "edit"):
			editFav(uid)
		elif (mode == "setMode" or mode == "setmode"):
			mo = params[3]
			db.fav.setModes(uid, ip, mo, mo, mo, mo)
		elif (mode == "test"):
			testFav(uid)
		else:
			bot.send_message(uid, "Unknown action, please consult /help")
	else:
		#printFav(uid)
		showFavMenu(m)
		

@bot.message_handler(func=lambda m: db.user.getState(m.chat.id) == "favMenu")
def comFavMenu(m):
	uid = m.chat.id
	msg = m.text
	
	if not msg[0].isdigit():
		bot.send_message(uid, "Not a valid command, please try again!")
	else:
		action = int(msg[0])
		if action == 0:
			db.user.setState(uid, "")
			#mainMenu()
			bot.reply_to(m, "Exiting...", reply_markup = hideBoard)
		elif action == 1:
			db.user.setState(uid, "favAdd")
			bot.reply_to(m, "Please enter the IP of the server you wish to add:", reply_markup=forceReply)
		elif action == 2:
			db.user.setState(uid, "favRm")
			bot.reply_to(m, "Please select the server you want to remove", reply_markup=getIpSel(uid, False))
		elif action == 3:
			db.user.setState(uid, "favNot")
			bot.reply_to(m, "Please select the server for which you want to change the notifications", reply_markup=getIpSel(uid, False))
		elif action == 4:
			bot.send_message(uid, "Work in Progress")
			
@bot.message_handler(func=lambda m: db.user.getState(m.chat.id) == "favAdd")
def comFavAdd(m):
	uid = m.chat.id
	msg = m.text
	if db.fav.add(uid, msg) >= 0:
		showFavMenu(m)
	else:
		invalidInput(uid)
	

@bot.message_handler(func=lambda m: db.user.getState(m.chat.id) == "favRm")
def comFavRm(m):
	uid = m.chat.id
	msg = m.text
	ip = processIpSel(uid, msg)
	if ip != -1:
		db.fav.remove(uid, ip)
		showFavMenu(m)


		
def enDisText(mode, inv): #return "Enable" or "Disable" based on boolean
	if inv:
		if mode == 1:
			return "enabled "+emoji.ok+"\n"
		else:
			return "disabled "+emoji.bad+"\n"
	else:
		if mode == 1:
			return " Disable"
		else:
			return " Enable"
	
def showNotStatus(uid, ip):
	mode = db.fav.getMode(uid, ip)
	modes = [mode["on"], mode["off"], mode["join"], mode["leave"]]
	status = emoji.on + " Online Notification " + enDisText(modes[0],1)
	status += emoji.off + " Offline Notification " + enDisText(modes[1],1)
	status += emoji.right + " Join Notification " + enDisText(modes[2],1)
	status += emoji.left + " Leave Notification " + enDisText(modes[3],1)
	bot.send_message(uid, "Current notification status: \n"+status)
	
@bot.message_handler(func=lambda m: db.user.getState(m.chat.id) == "favNot")
def comFavNot(m):
	uid = m.chat.id
	msg = m.text
	ip = processIpSel(uid, msg)
	if ip != -1:
		lastIpSelect[uid] = ip
		mode = db.fav.getMode(uid, ip)
		modes = [mode["on"], mode["off"], mode["join"], mode["leave"]]
		favNotMenu = types.ReplyKeyboardMarkup(selective=True)
		favNotMenu.row("0. "+emoji.back+" Favorite Menu")
		favNotMenu.row("1. Enable All", "2. Disable All")
		favNotMenu.row("3. "+emoji.on + enDisText(modes[0],0) + " Online Notification", "4. "+emoji.off + enDisText(modes[1],0) + " Offline Notification")
		favNotMenu.row("5. "+emoji.right + enDisText(modes[2],0) + " Join Notification", "6. "+emoji.left + enDisText(modes[3],0) + " Leave Notification")
		showNotStatus(uid, ip)
		bot.reply_to(m, "What would you like to do?", reply_markup=favNotMenu)
		db.user.setState(uid, "favNotSelect")
		#showFavMenu(m)

@bot.message_handler(func=lambda m: db.user.getState(m.chat.id) == "favNotSelect")
def comFavNotSelect(m):
	uid = m.chat.id
	msg = m.text
	try:
		ip = lastIpSelect[uid]
	except KeyError:
		bot.send_message(uid, "Oops, something went wrong... Returning to favorite menu...")
		showFavMenu(m)
	else:
		try:
			action = int(msg[0])
		except:
			invalidInput(uid)
		else:
			mo = ()
			if action == 0:
				showFavMenu(m)
			else:
				curMo = db.fav.getMode(uid, ip)
				if action == 1:
					mo = (1,1,1,1)
				elif action == 2:
					mo = (0,0,0,0)
				elif action == 3:
					mo = (not curMo["on"], curMo["off"], curMo["join"], curMo["leave"])
				elif action == 4:
					mo = (curMo["on"], not curMo["off"], curMo["join"], curMo["leave"])
				elif action == 5:
					mo = (curMo["on"], curMo["off"], not curMo["join"], curMo["leave"])
				elif action == 6:
					mo = (curMo["on"], curMo["off"], curMo["join"], not curMo["leave"])
				
				db.fav.setModes(uid, ip, mo[0], mo[1], mo[2], mo[3])
				showNotStatus(uid, ip)
				showFavMenu(m)
				
		
@bot.message_handler(func=lambda m: user.getState(m.chat.id) == "favEdit")
def comFavEdit(m):
	uid = m.chat.id
	msg = m.text
	showFavMenu(m)
	

#= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
#= = = = = = = = = = = = = = = = = = = = =     Server Poller     = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
#= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
"""
def sendNotifications(): #sends the notifications, that the poller create
	msgs = dict(poller.getNotificationMsgs())
	try:
		if msgs["fresh"] == True:
			print msgs
			del msgs["fresh"]
			for uid in msgs:
				msg = msgs[uid]
				print uid, msg
				bot.send_message(uid, msg)
			poller.clrMsgs()
	except KeyError:
		pass"""
			

#= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 	
try:	
	while True:
		time.sleep(1)
		#sendNotifications()
except:
	#print e
	print "Closing database"
	db.close()
	#break
