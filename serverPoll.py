﻿from threading import Thread
import threading
import time

import settings

from emojis import *
emoji = emoji()

class poller:
	def __init__(self, db, server, bot):
		self.db = db
		self.server = server
		self.bot = bot
		self._msgs = {}
		
	def getJoined(self, lastUsers, curUsers):
		return list(set(curUsers) - set(lastUsers))
	def getLeft(self, lastUsers, curUsers):
		return list(set(lastUsers) - set(curUsers))
		
		
	def getChanges(self, ip):
		curData = settings.sData[ip]
		if curData["online"]:
			try:
				lastData = self.db.lastData.get(ip)
				#print sData[ip]["uNames"], lastData["uNames"]
				changes = {"left": [], "joined": [], "wentOnline": False, "wentOffline": False}
				if lastData["online"]==False and curData["online"] == True:
					changes["wentOnline"] = True #Server went online
				elif lastData["online"]==True and curData["online"] == False:
					changes["wentOffline"] = True
				else:
					#changes["wentOnline"] = False
					#changes["wentOffline"] = False
					changes["left"] = self.getLeft(lastData["uNames"], curData["uNames"])
					changes["joined"] = self.getJoined(lastData["uNames"], curData["uNames"])
				self.db.lastData.set(ip, curData)
				return changes
			except KeyError as e:
				self.db.lastData.set(ip, curData)
				print "KeyError",e,ip,"in getChanges()"
				return None
		else:
			self.db.lastData.set(ip, {'online': False, 'uNames': []})
			return None
		
		
	def notify(self):
		notifies = {}
		changes = {}
		
		for ip in self.db.fav.getNotifyIPs():
			c = self.getChanges(ip)
			#print ip, c
			if c != None:
				#print ip, getSubscribers(ip)
				for uid in self.db.fav.getSubscribers(ip):
					cMsg = ""
					mode = self.db.fav.getMode(uid, ip)
					if c["wentOffline"]:
						#print ip, "went offline"
						if mode["off"]:
							cMsg += "Server [" + ip.encode('utf-8') + "] went offline! "+ emoji.off
					if c["wentOnline"]:
						#print ip, "went online"
						if mode["on"]:
							cMsg += "Server [" + ip.encode('utf-8') + "] went back online! " + emoji.on
					for leaver in c["left"]:
						if leaver != None:
							#print leaver, "left",ip
							if mode["leave"]:
								cMsg += "\n<" + leaver + "> left [" + ip + "]"
					for joiner in c["joined"]:
						if joiner != None:
							#print joiner, "joined",ip
							if mode["join"]:
								cMsg += "\n<" + joiner + "> joined [" + ip + "]"
					
					if cMsg != "":
						self.bot.send_message(uid, cMsg)
						"""self.bot(uid, cMsg)
						#self._msgs[uid] = cMsg
		self._msgs["fresh"] = True
		
	def getNotificationMsgs(self):
		return self._msgs
	def clrMsgs(self):
		self._msgs = {}"""

	def update(self):
		servers = self.db.fav.getIPs()
		threads = []
		for ip in servers:
			pingThread = Thread(target=self.server.ping, args=(ip,))
			pingThread.start()
			threads.append(pingThread)
		for thread in threads:
			thread.join()

	def updater(self):
		while True:
			pTime = time.time()
			#print "updating"
			self.update()
			self.notify()
			sleep = settings.pollInterval-(time.time() - pTime) #compensate for update duration
			if sleep > 0:
				time.sleep(sleep)

	def start(self):
		updateThread = Thread(target=self.updater)	
		updateThread.daemon = True
		updateThread.start()	