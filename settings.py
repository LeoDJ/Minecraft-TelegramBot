﻿# -*- coding: utf-8 -*-

TOKEN = '<token>'
dbPath = 'mDB.sqlite'
feedbackFile = 'feedback.txt'
pollInterval = 15

aboutText = "I was created by @LeoDJ"

helpTextPre = "Available commands:"
helpTextAfter = "\n\nSymbol declaration: \n🔵/🔴 = Server online/offline\n❌/🔕/🔔 = Notifications unavailable/disabled/enabled"

commands=( #command description used in the "help" command
('start', 'Get started'),
('ping [IP|fav]', 'Give information about a Server (Players, MOTD, Version)'),
('fav', 'List your favorites'),
('fav add [IP]', 'Add a server to your favorites'),
('fav remove [IP|fav]', 'Remove a server from your favorites'),
('fav edit [IP|fav]', '[WIP] Edit a server from your favorites'),
('help', 'Gives you information about the available commands'),
('feedback', 'Send me feedback'),
('about', 'About me')
)

def init():
	global sData
	sData = {}
	
def isAnySet(dict): #check if any value in dict is set to 1
	for v in dict:
		if dict[v] == 1:
			return True
	return False
	
def isValidIP(ip):
	valid = True
	try:
		ip.decode('utf-8')
	except UnicodeEncodeError:
		valid = False
	#if '?' in ip:
	#	valid = False
	#elif '/' in ip:
	#	valid = False
	print ip,valid
	return valid