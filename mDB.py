﻿# -*- coding: utf-8 -*-
import sqlite3
import threading
from threading import Thread
import threading
from Queue import Queue
from telebot.types import *

import settings
from settings import *

class MultiThreadOK(Thread):
	def __init__(self, db):
		super(MultiThreadOK, self).__init__()
		self.db=db
		self.reqs=Queue()
		self.start()
	def run(self):
		#cnx = apsw.Connection(self.db) 
		cnx = sqlite3.connect(self.db)
		cursor = cnx.cursor()
		while True:
			req, arg, res = self.reqs.get()
			if req=='--close--': break
			try:
				cursor.execute(req, arg)
			except:
				print "Executing request: ",req,arg  #DEBUG only
			cnx.commit()
			if res:
				for rec in cursor:
					res.put(rec)
				res.put('--no more--')
		cnx.close()
	def execute(self, req, arg=None, res=None):
		self.reqs.put((req, arg or tuple(), res))
	def select(self, req, arg=None):
		res=Queue()
		self.execute(req, arg, res)
		while True:
			rec=res.get()
			if rec=='--no more--': break
			yield rec
	def close(self):
		self.execute('--close--')

#global sql		
#sql = MultiThreadOK(dbPath)

def createTables(sql):
	sql.execute('''
		CREATE TABLE IF NOT EXISTS users
		(
			id 			INTEGER NOT NULL, 
			username 	TEXT,
			isOp 		INTEGER,
			first_name 	TEXT,
			last_name 	TEXT,
			state 		TEXT,
			PRIMARY KEY(id)
		);
				''')
	sql.execute('''
		CREATE TABLE IF NOT EXISTS favs
		(
			id 				INTEGER NOT NULL,
			userID 			INTEGER,
			pos 			INTEGER UNIQUE,
			ip 				TEXT,
			notifyEnabled	INTEGER NOT NULL, 
			notifyOnline	INTEGER NOT NULL,
			notifyOffline	INTEGER NOT NULL,
			notifyJoin		INTEGER NOT NULL,
			notifyLeave		INTEGER NOT NULL,
			PRIMARY KEY(id)
			FOREIGN KEY(userID) REFERENCES users(id)
		);
				''')
	sql.execute('''
		CREATE TABLE IF NOT EXISTS lastData (
			ip		TEXT NOT NULL,
			online	INTEGER,
			player	TEXT
		);
				''')
			
	

	
#= = = = = = = = = = = = = = = = = = = = =     Main     = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
class connect:
	def __init__(self, dbPath):
		self.sql = MultiThreadOK(dbPath)
		self.user = user(self.sql)
		self.fav = fav(self.sql)
		self.lastData = lastData(self.sql)
		createTables(self.sql)
	
	def select(self, params):	
		return self.sql.select(params)
	def execute(self, params):
		self.sql.execute(params)
	def close(self):
		self.sql.close()
		

		
#= = = = = = = = = = = = = = = = = = = = =     User     = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
class user:
	def __init__(self, sqlObj):	
		self.sql = sqlObj
		
	def getState(self, uid):
		for st in self.sql.select("SELECT state FROM users WHERE id = ?", (uid,)):
			return st[0]
		
	def setState(self, uid, state):
		self.sql.execute("UPDATE users SET state = ? WHERE id = ?", (state, uid))
		
	def isRegistered(self, uid):
		tmp = False
		for u in self.sql.select("SELECT id FROM users WHERE id = "+str(uid)):
			tmp = True
		return tmp
		
	def register(self, chat):
		if isinstance(chat, User):
			self.sql.execute("INSERT OR REPLACE INTO users VALUES(?,?,?,?,?,?)", (chat.id, chat.username, 0, chat.first_name, chat.last_name, 0))
		else:
			self.sql.execute("INSERT OR REPLACE INTO users VALUES(?,?,?,?,?,?)", (chat.id, chat.title, 0, None, None, 0))


		
#= = = = = = = = = = = = = = = = = = = = =     Fav     = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
class fav:
	def __init__(self, sqlObj):	
		self.sql = sqlObj
		
	def getIP(self, uid, pos):
		for f in self.sql.select("SELECT ip FROM favs WHERE userID = ? AND pos = ?", (uid, pos)):
			return f[0]
		
	def getPos(self, uid, ip):
		for p in self.sql.select("SELECT pos FROM favs WHERE userID = ? AND ip = ?", (uid, ip)):
			return int(p[0])
			
	def getMode(self, uid, ip):
		for m in self.sql.select("SELECT notifyEnabled, notifyOnline, notifyOffline, notifyJoin, notifyLeave FROM favs WHERE userID = ? AND ip = ?", (uid, ip)):
			mode = {}
			mode["enabled"] = m[0]
			mode["on"] = m[1]
			mode["off"] = m[2]
			mode["join"] = m[3]
			mode["leave"] = m[4]
			return mode
			
	def get(self, uid):
		favs = []
		for f in self.sql.select("SELECT pos, ip FROM favs WHERE userID = ? ORDER BY pos", (uid,)):
			pos = str(f[0])
			ip = f[1].encode('utf-8')
			mode = self.getMode(uid, ip)
			favs.append([pos, ip, mode])
		return favs	
		
	def add(self, uid, ip):
		maxPos = 0
		for p in self.sql.select("SELECT pos FROM favs WHERE userID = ?", (uid,)):
			pos = int(p[0])
			if pos > maxPos:
				maxPos = pos
		if settings.isValidIP(ip):
			self.sql.execute("INSERT INTO favs (userID, pos, ip, notifyEnabled, notifyOnline, notifyOffline, notifyJoin, notifyLeave) VALUES (?,?,?,0,0,0,0,0)", (uid, maxPos+1, ip.lower()))
			return 0
		else:
			return -1
		#printFav(uid)
	
	def remove(self, uid, ip):
		pos = self.getPos(uid, ip)
		self.sql.execute("DELETE FROM favs WHERE userID = ? AND ip = ?", (uid, ip))
		self.sql.execute("UPDATE favs SET pos = pos - 1 WHERE pos > ?", (pos,))
		#printFav(uid)
		
	def setMode(self, uid, ip, mo):
		
		self.sql.execute("UPDATE favs SET notifyEnabled=?, notifyOnline=?, notifyOffline=?, notifyJoin=?, notifyLeave=? WHERE userID = ? AND ip = ?", (settings.isAnySet(mo), mo["on"], mo["off"], mo["join"], mo["leave"], uid, ip))

	def setModes(self, uid, ip, moOn, moOff, moJoin, moLeave):
		mode = {}
		mode["on"] = moOn
		mode["off"] = moOff
		mode["join"] = moJoin
		mode["leave"] = moLeave
		self.setMode(uid, ip, mode)
		
	def getSubscribers(self, ip):
		return [s[0] for s in self.sql.select("SELECT userID FROM favs WHERE ip = ? AND notifyEnabled = 1", (ip,))]

	def getIPs(self):
		return [ip[0] for ip in self.sql.select("SELECT ip FROM favs GROUP BY ip") ]
		
	def getNotifyIPs(self):
		return [ip[0] for ip in self.sql.select("SELECT ip FROM favs WHERE notifyEnabled = 1 GROUP BY ip") ]

		
#= = = = = = = = = = = = = = = = = = = = =     LastData     = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 
class lastData:
	def __init__(self, sqlObj):	
		self.sql = sqlObj
		
	def get(self, ip):
		lData = {"uNames": []}
		for o in self.sql.select("SELECT online FROM lastData WHERE ip = ? GROUP BY ip", (ip,)):
			lData["online"] = o[0]
		for p in self.sql.select("SELECT player FROM lastData WHERE ip = ?", (ip,)):
			lData["uNames"].append(p[0])
		return lData
			
	def set(self, ip, data):
		self.sql.execute("DELETE FROM lastData WHERE ip = ?", (ip,))
		self.sql.execute("INSERT INTO lastData (ip, online) VALUES (?,?)", (ip, data["online"]))
		for player in data["uNames"]:
			#self.sql.execute("DELETE FROM lastData WHERE ip = ? AND online = ? AND player = ?", (ip, data["online"], player))
			self.sql.execute("INSERT INTO lastData VALUES (?,?,?)", (ip, data["online"], player))
		
	