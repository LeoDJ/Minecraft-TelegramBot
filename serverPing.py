﻿from mcstatus import MinecraftServer
import socket

import settings


class server:
	def __init__(self, dbObj):
		self.db = dbObj
		self.timeOutCount = {}

	def ping(self, ip):
		try:
			data = settings.sData[ip]
		except Exception as e:
			data = {}
			
		try:
			srv = MinecraftServer.lookup(ip)
			#print "lookup done"
			st = srv.status(3)
			data["online"] = True
			self.timeOutCount[ip] = 0
			data["ping"] = int(st.latency)
			data["version"] = st.version.name
			data["motd"] = st.description
			data["uOnline"] = st.players.online
			data["uMax"] = st.players.max
			try:
				data["uNames"] = [p.name for p in st.players.sample]
				data["uNamesSample"] = True
			except:
				data["uNames"] = []
				data["uNamesSample"] = False
			
			#try:
			#	data["ping"] = int(srv.ping(1))
			#except Exception as e:
			#	#print e
			#	data["ping"] = "--- "
				
			try:
				q = srv.query(1)
				data["queryable"] = True
				data["brand"] = q.software.brand
				data["mods"] = q.software.plugins
				data["uNames"] = q.players.names
				data["uNamesSample"] = False
			except Exception as e:
				#print e
				data["queryable"] = False
				data["brand"] = ""
				data["mods"] = []
				#data["uNames"] = {}
				
			#print "Successfully pinged",ip
			
			returnVal = 0
		except socket.timeout:
			#print "time out:",ip
			try:
				if self.timeOutCount[ip] < 3:
					self.timeOutCount[ip] += 1
					print ip, "timeout",self.timeOutCount[ip]
					return 0
				else:
					data["online"] = False
					returnVal = -1
			except KeyError:
				data["online"] = False
				returnVal = -1
		except Exception as e:
			#print ip, e
			#if e == "timed out" and self.timeOutCount[ip] < 5:
			#	print ip, "resultet in timeout", self.timeOutCount[ip]
			#	data["online"] = True
			#	self.timeOutCount[ip] += 1
			#else:
			#if ip == '85.114.153.172' or ip == 'darkus.selfhost.me:25569':
			#	print ip, e, e.errno, e.strerror, e.message
			data["online"] = False
			returnVal = -1
		settings.sData[ip] = data
		return returnVal
		
	def fixMOTD(self, motd):
		if (motd[0] != u'§'):	#prevent cutting off the first character if it's not a §
			motd = " " + motd
		motd = motd.split(u"§")
		newMotd = u""
		for t in motd:
			#print t[1:].encode('utf-8')
			newMotd += t[1:]
		return newMotd
			

	def getData(self, ip):
		#print sData
		data = settings.sData[ip]
		dataString = "IP: " + ip + " (Ping: " + str(data["ping"]) + "ms)"
		dataString += "\nMOTD: " + self.fixMOTD(data["motd"])
		dataString += "\nVersion: " + data["version"]
		dataString += "\n\n" + str(data["uOnline"]) + "/" + str(data["uMax"]) + " players online:"
		dataString += "\n" + ", ".join(data["uNames"])
		if data["uNamesSample"]:
			dataString += " ..."
		return dataString